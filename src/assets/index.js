import logo from "./logo.svg";
import gs from "./gs.svg";
import backend from "./backend.png";
import creator from "./creator.png";
import mobile from "./mobile.png";
import web from "./web.png";
import github from "./github.png";
import gitlab from "./gitlab.svg";
import menu from "./menu.svg";
import close from "./close.svg";

import css from "./tech/css.png";
import beekeeper from "./tech/beekeeper.svg";
import docker from "./tech/docker.png";
import figma from "./tech/figma.png";
import git from "./tech/git.png";
import html from "./tech/html.png";
import javascript from "./tech/javascript.png";
import mongodb from "./tech/mongodb.png";
import nodejs from "./tech/nodejs.png";
import reactjs from "./tech/reactjs.png";
import redux from "./tech/redux.png";
import tailwind from "./tech/tailwind.png";
import typescript from "./tech/typescript.png";
import threejs from "./tech/threejs.svg";
import django from "./tech/django.png";
import FastaApi from "./tech/FastaApi.svg";
import insomnia from "./tech/insomnia.png";
import PostgreSQL from "./tech/PostgreSQL.png";
import SQL from "./tech/SQL.svg";


import meta from "./company/meta.png";
import shopify from "./company/shopify.png";
import starbucks from "./company/starbucks.png";
import tesla from "./company/tesla.png";
import hr from "./company/hr.png";

import carrent from "./carrent.png";
import jobit from "./jobit.png";
import tripguide from "./tripguide.png";
import conference from "./conference.jpeg";
import Autozone from "./Autozone.png";
import Tracker from "./Tracker.jpg";
import HealthHustlers from "./HealthHustlers.png";
import wardrobify from "./wardrobify.png";



export {
  logo,
  gs,
  backend,
  creator,
  mobile,
  web,
  github,
  gitlab,
  menu,
  close,
  css,
  beekeeper,
  docker,
  figma,
  git,
  html,
  javascript,
  mongodb,
  nodejs,
  reactjs,
  redux,
  tailwind,
  typescript,
  threejs,
  meta,
  shopify,
  starbucks,
  tesla,
  hr,
  carrent,
  jobit,
  tripguide,
  django,
  FastaApi,
  insomnia,
  PostgreSQL,
  SQL,
  conference,
  Autozone,
  Tracker,
  HealthHustlers,
  wardrobify,
};
