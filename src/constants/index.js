import {
  mobile,
  backend,
  creator,
  hr,
  web,
  javascript,
  typescript,
  html,
  css,
  reactjs,
  redux,
  tailwind,
  nodejs,
  mongodb,
  git,
  figma,
  docker,
  meta,
  starbucks,
  tesla,
  shopify,
  carrent,
  jobit,
  tripguide,
  threejs,
  beekeeper,
  django,
  FastaApi,
  insomnia,
  PostgreSQL,
  SQL,
  conference,
  HealthHustlers,
  Tracker,
  Autozone,
  wardrobify,

} from "../assets";

export const navLinks = [
  {
    id: "about",
    title: "About",
  },
  {
    id: "work",
    title: "Work",
  },
  {
    id: "contact",
    title: "Contact",
  },
];

const services = [
  {
    title: "Frontend Developer",
    icon: web,
  },

  {
    title: "Backend Developer",
    icon: backend,
  },
  {
    title: "Software Engineer",
    icon: creator,
  },
];

const technologies = [
  {
    name: "HTML 5",
    icon: html,
  },
  {
    name: "CSS 3",
    icon: css,
  },
  {
    name: "JavaScript",
    icon: javascript,
  },
  {
    name: "React JS",
    icon: reactjs,
  },
  {
    name: "Redux Toolkit",
    icon: redux,
  },
  {
    name: "Tailwind CSS",
    icon: tailwind,
  },
  {
    name: "Node JS",
    icon: nodejs,
  },
  {
    name: "MongoDB",
    icon: mongodb,
  },
  {
    name: "git",
    icon: git,
  },
  {
    name: "docker",
    icon: docker,
  },
  {
    name: "beekeeper",
    icon: beekeeper,
  },
  // {
  //   name: "django",
  //   icon: django,
  // },
  {
    name: "FastaApi",
    icon: FastaApi,
  },
  {
    name: "insomnia",
    icon: insomnia,
  },
  {
    name: "PostgreSQL",
    icon: PostgreSQL,
  },
  // {
  //   name: "SQL",
  //   icon: SQL,
  // },
];

const experiences = [
  {
    title: "Tracker",
    company_name: "Hack Reactor",
    icon: hr,
    iconBg: "#383E56",
    date: "Feb 2023 - Mar 2023",
    points: [
      " Designed SQL database models and schema in Django.",
      "Implemented a backend user authentication system with front end user sign up and login forms.",
      "Provided CRUD operations to modify user created projects and tasks.",
    ],
  },
  {
    title: "Conference GO!",
    company_name: "Hack Reactor",
    icon: hr,
    iconBg: "#E6DEDD",
    date: "Mar 2023 - Apr 2023",
    points: [
      "Developed and designed an conference management application utilizing Django, Python, CSS, and JSON.",
      "Integrated third party APIs to include a photo based on the inputted location and incorporate weather data for the users.",
      "Streamlined accessibility and improved maintainability of the application by refactoring from a monolithic to microservices based application.",
    ],
  },
  {
    title: "Wardrobify",
    company_name: "Hack Reactor",
    icon: hr,
    iconBg: "#383E56",
    date: "Apr 2023- May 2023",
    points: [
      "Used React for the front-end and Django for the back-end, enabling users to efficiently sort and manage their collection.",
      "Utilized React forms to enable users to easily add new hats and shoes to their wardrobe, improving the overall user experience and streamlining the data input process.",
      "Implemented robust functionality for deleting shoes and hats, enhancing the app's versatility and allowing users to easily manage and update their wardrobe collection.",
    ],
  },
  {
    title: "AutoZone",
    company_name: "Hack Reactor",
    icon: hr,
    iconBg: "#383E56",
    date: "Apr 2023 - May 2023",
    points: [
      "Created poller to receive dynamic inventory data into a service microservice for vehicle information.",
      "Containerized microservices intercommunication using docker.",
      "Implemented web forms with React to manage automobiles sales, repairs service, and inventory.",
    ],
  },
  {
    title: "Health Hustlers",
    company_name: "Hack Reactor",
    icon: hr,
    iconBg: "#383E56",
    date: "May 2023 - Present",
    points: [
      "Collaborated with a team to design the frontend and backend flow using wireframes to create an intuitive user experience.",
      "Implemented secure authorization for both the frontend and backend using JSON Web Tokens, ensuring appropriate access based on user roles.",
      "Deployed the application on Galvanize Cloud and utilized GitLab CI/CD pipelines for efficient code review, merge requests, and passing pipeline tests.",
    ],
  },
];

const testimonials = [
  {
    testimonial:
      "I thought it was impossible to make a website as beautiful as our product, but Rick proved me wrong.",
    name: "Sara Lee",
    designation: "CFO",
    company: "Acme Co",
    image: "https://randomuser.me/api/portraits/women/4.jpg",
  },
  {
    testimonial:
      "I've never met a web developer who truly cares about their clients' success like Rick does.",
    name: "Chris Brown",
    designation: "COO",
    company: "DEF Corp",
    image: "https://randomuser.me/api/portraits/men/5.jpg",
  },
  {
    testimonial:
      "After Rick optimized our website, our traffic increased by 50%. We can't thank them enough!",
    name: "Lisa Wang",
    designation: "CTO",
    company: "456 Enterprises",
    image: "https://randomuser.me/api/portraits/women/6.jpg",
  },
];

const projects = [
  {
    name: "Tracker",
    description:
      "This is a user friendly web application which allows users to manage tasks related to a project.",
    tags: [
      {
        name: "Python",
        color: "blue-text-gradient",
      },
      {
        name: "HTML",
        color: "green-text-gradient",
      },
      {
        name: "CSS",
        color: "pink-text-gradient",
      },
    ],
    image: Tracker,
    source_code_link: "https://gitlab.com/gurjinder124/tracker",
  },
  {
    name: "Conference GO",
    description:
      "Conference GO is a conference management solution, streamlining registrations, scheduling, and managing the presentations and locations of the conferences.",
    tags: [
      {
        name: "Javascript",
        color: "blue-text-gradient",
      },
      {
        name: "Django",
        color: "green-text-gradient",
      },
      {
        name: "Bootstrap",
        color: "pink-text-gradient",
      },
    ],
    image:conference,
    source_code_link: "https://gitlab.com/gurjinder124/conference-go",
  },
  {
    name: "Wardrobify",
    description:
      "Wardrobify is a user-friendly app that serves as a virtual wardrobe for storing hats and shoes, enabling users to effortlessly explore and navigate their hat and shoe collections.",
    tags: [
      {
        name: "Javascript",
        color: "blue-text-gradient",
      },
      {
        name: "Django",
        color: "green-text-gradient",
      },
      {
        name: "Bootstrap",
        color: "pink-text-gradient",
      },
    ],
    image:wardrobify,
    source_code_link: "https://gitlab.com/Krze44/wardrobify",
  },
  {
    name: "AutoZone",
    description:
      "AutoZone is a web application for managing automobile dealership—specifically its inventory, service center, and sales.",
    tags: [
      {
        name: "Django",
        color: "blue-text-gradient",
      },
      {
        name: "React",
        color: "green-text-gradient",
      },
      {
        name: "css",
        color: "pink-text-gradient",
      },
    ],
    image: Autozone,
    source_code_link: "https://gitlab.com/gurjinder124/AutoZone",
  },
  {
    name: "Health Hustlers",
    description:
      "Health Hustlers is a comprehensive full stack application for tracking workouts and managing meal intake.",
    tags: [
      {
        name: "FastAPI",
        color: "blue-text-gradient",
      },
      {
        name: "React",
        color: "green-text-gradient",
      },
      {
        name: "Tailwind",
        color: "pink-text-gradient",
      },
    ],
    image: HealthHustlers,
    source_code_link: "https://gitlab.com/health-hustlers/module3-project-gamma",
  },
];

export { services, technologies, experiences, testimonials, projects };
