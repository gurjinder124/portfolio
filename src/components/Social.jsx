import React from "react";
import { SectionWrapper } from "../hoc";
import { Container } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faTwitter, faInstagram, faLinkedin, faGitlab } from '@fortawesome/free-brands-svg-icons';
import "./Social.css";

const Social = () => {
  return (
    <Container>
      <footer
        className="bg-white rounded-lg z-20"
        style={{
            position: "fixed",
            bottom: 0,
            right: 0,
            left: 0,
          background: "linear-gradient(to right, #552586, #6A359C, #804FB3, #9969C7, #B589D6) 0px 0px / cover no-repeat",
        }}
      >
        <div className="w-full p-4 md:flex md:items-center md:justify-between">
          <span className="text-sm text-white sm:text-center dark:text-white">
            © All Rights Reserved.
          </span>
          <span className="text-sm text-white sm:text-center dark:text-white">
            Developed by <strong>Gurjinder Singh</strong>
          </span>
          <div className="singleCol sociaL-media-icons-white d-flex justify-content-evenly">
            <a href="https://www.facebook.com/gurjinder124/">
              <FontAwesomeIcon icon={faFacebook} />
            </a>
            <a href="https://twitter.com/Gurjind77285334">
              <FontAwesomeIcon icon={faTwitter} />
            </a>
            <a href="https://www.instagram.com/gurjinder12/">
              <FontAwesomeIcon icon={faInstagram} />
            </a>
            <a href="https://www.linkedin.com/in/gurjinder124/">
              <FontAwesomeIcon icon={faLinkedin} />
            </a>
            <a href="https://gitlab.com/gurjinder124">
              <FontAwesomeIcon icon={faGitlab} />
            </a>
          </div>
        </div>
      </footer>
    </Container>
  );
};

export default SectionWrapper(Social, "social");
